﻿namespace Estado_del_Mar
{
    partial class EstadoDelMar
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BT_TemperaturaAmbiente = new System.Windows.Forms.Button();
            this.BT_TemperaturaMar = new System.Windows.Forms.Button();
            this.BT_EstadoMar = new System.Windows.Forms.Button();
            this.BT_VelovidadViento = new System.Windows.Forms.Button();
            this.BT_PresenciaMedusas = new System.Windows.Forms.Button();
            this.BT_PresenciaLluvias = new System.Windows.Forms.Button();
            this.Historial = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.LB_VelocidadViento = new System.Windows.Forms.Label();
            this.LB_EspecieMedusas = new System.Windows.Forms.Label();
            this.LB_PresenciaMedusas = new System.Windows.Forms.Label();
            this.LB_EstadoMar = new System.Windows.Forms.Label();
            this.LB_PresenciaLluvia = new System.Windows.Forms.Label();
            this.LB_TemperaturaAmbiente = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.LB_TemperaturaMar = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.LB_UltimaActualizacion = new System.Windows.Forms.Label();
            this.BT_ActualizarDatos = new System.Windows.Forms.Button();
            this.Historial.SuspendLayout();
            this.SuspendLayout();
            // 
            // BT_TemperaturaAmbiente
            // 
            this.BT_TemperaturaAmbiente.Location = new System.Drawing.Point(36, 24);
            this.BT_TemperaturaAmbiente.Name = "BT_TemperaturaAmbiente";
            this.BT_TemperaturaAmbiente.Size = new System.Drawing.Size(76, 48);
            this.BT_TemperaturaAmbiente.TabIndex = 0;
            this.BT_TemperaturaAmbiente.Text = "Temperatura Ambiente";
            this.BT_TemperaturaAmbiente.UseVisualStyleBackColor = true;
            this.BT_TemperaturaAmbiente.Click += new System.EventHandler(this.BT_TemperaturaAmbiente_Click);
            // 
            // BT_TemperaturaMar
            // 
            this.BT_TemperaturaMar.Location = new System.Drawing.Point(36, 77);
            this.BT_TemperaturaMar.Name = "BT_TemperaturaMar";
            this.BT_TemperaturaMar.Size = new System.Drawing.Size(76, 48);
            this.BT_TemperaturaMar.TabIndex = 1;
            this.BT_TemperaturaMar.Text = "Temperatura del mar";
            this.BT_TemperaturaMar.UseVisualStyleBackColor = true;
            this.BT_TemperaturaMar.Click += new System.EventHandler(this.BT_TemperaturaMar_Click);
            // 
            // BT_EstadoMar
            // 
            this.BT_EstadoMar.Location = new System.Drawing.Point(36, 183);
            this.BT_EstadoMar.Name = "BT_EstadoMar";
            this.BT_EstadoMar.Size = new System.Drawing.Size(76, 48);
            this.BT_EstadoMar.TabIndex = 2;
            this.BT_EstadoMar.Text = "Estado del mar (Douglas)";
            this.BT_EstadoMar.UseVisualStyleBackColor = true;
            this.BT_EstadoMar.Click += new System.EventHandler(this.BT_EstadoMar_Click);
            // 
            // BT_VelovidadViento
            // 
            this.BT_VelovidadViento.Location = new System.Drawing.Point(36, 130);
            this.BT_VelovidadViento.Name = "BT_VelovidadViento";
            this.BT_VelovidadViento.Size = new System.Drawing.Size(76, 48);
            this.BT_VelovidadViento.TabIndex = 3;
            this.BT_VelovidadViento.Text = "Velocidad del viento";
            this.BT_VelovidadViento.UseVisualStyleBackColor = true;
            this.BT_VelovidadViento.Click += new System.EventHandler(this.BT_VelovidadViento_Click);
            // 
            // BT_PresenciaMedusas
            // 
            this.BT_PresenciaMedusas.Location = new System.Drawing.Point(36, 236);
            this.BT_PresenciaMedusas.Name = "BT_PresenciaMedusas";
            this.BT_PresenciaMedusas.Size = new System.Drawing.Size(76, 48);
            this.BT_PresenciaMedusas.TabIndex = 4;
            this.BT_PresenciaMedusas.Text = "Presencia Medusas";
            this.BT_PresenciaMedusas.UseVisualStyleBackColor = true;
            this.BT_PresenciaMedusas.Click += new System.EventHandler(this.BT_PresenciaMedusas_Click);
            // 
            // BT_PresenciaLluvias
            // 
            this.BT_PresenciaLluvias.Location = new System.Drawing.Point(36, 289);
            this.BT_PresenciaLluvias.Name = "BT_PresenciaLluvias";
            this.BT_PresenciaLluvias.Size = new System.Drawing.Size(76, 48);
            this.BT_PresenciaLluvias.TabIndex = 4;
            this.BT_PresenciaLluvias.Text = "Presencia Lluvias";
            this.BT_PresenciaLluvias.UseVisualStyleBackColor = true;
            this.BT_PresenciaLluvias.Click += new System.EventHandler(this.BT_PresenciaLluvias_Click);
            // 
            // Historial
            // 
            this.Historial.Controls.Add(this.BT_TemperaturaAmbiente);
            this.Historial.Controls.Add(this.BT_PresenciaLluvias);
            this.Historial.Controls.Add(this.BT_TemperaturaMar);
            this.Historial.Controls.Add(this.BT_PresenciaMedusas);
            this.Historial.Controls.Add(this.BT_VelovidadViento);
            this.Historial.Controls.Add(this.BT_EstadoMar);
            this.Historial.Location = new System.Drawing.Point(583, 47);
            this.Historial.Name = "Historial";
            this.Historial.Size = new System.Drawing.Size(171, 347);
            this.Historial.TabIndex = 5;
            this.Historial.TabStop = false;
            this.Historial.Text = "Historial";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(48, 75);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(104, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Última actualización:";
            // 
            // LB_VelocidadViento
            // 
            this.LB_VelocidadViento.AutoSize = true;
            this.LB_VelocidadViento.Location = new System.Drawing.Point(153, 171);
            this.LB_VelocidadViento.Name = "LB_VelocidadViento";
            this.LB_VelocidadViento.Size = new System.Drawing.Size(35, 13);
            this.LB_VelocidadViento.TabIndex = 6;
            this.LB_VelocidadViento.Text = "label2";
            // 
            // LB_EspecieMedusas
            // 
            this.LB_EspecieMedusas.AutoSize = true;
            this.LB_EspecieMedusas.Location = new System.Drawing.Point(153, 224);
            this.LB_EspecieMedusas.Name = "LB_EspecieMedusas";
            this.LB_EspecieMedusas.Size = new System.Drawing.Size(35, 13);
            this.LB_EspecieMedusas.TabIndex = 6;
            this.LB_EspecieMedusas.Text = "label3";
            // 
            // LB_PresenciaMedusas
            // 
            this.LB_PresenciaMedusas.AutoSize = true;
            this.LB_PresenciaMedusas.Location = new System.Drawing.Point(153, 207);
            this.LB_PresenciaMedusas.Name = "LB_PresenciaMedusas";
            this.LB_PresenciaMedusas.Size = new System.Drawing.Size(35, 13);
            this.LB_PresenciaMedusas.TabIndex = 6;
            this.LB_PresenciaMedusas.Text = "label4";
            // 
            // LB_EstadoMar
            // 
            this.LB_EstadoMar.AutoSize = true;
            this.LB_EstadoMar.Location = new System.Drawing.Point(153, 189);
            this.LB_EstadoMar.Name = "LB_EstadoMar";
            this.LB_EstadoMar.Size = new System.Drawing.Size(35, 13);
            this.LB_EstadoMar.TabIndex = 6;
            this.LB_EstadoMar.Text = "label5";
            // 
            // LB_PresenciaLluvia
            // 
            this.LB_PresenciaLluvia.AutoSize = true;
            this.LB_PresenciaLluvia.Location = new System.Drawing.Point(153, 241);
            this.LB_PresenciaLluvia.Name = "LB_PresenciaLluvia";
            this.LB_PresenciaLluvia.Size = new System.Drawing.Size(35, 13);
            this.LB_PresenciaLluvia.TabIndex = 6;
            this.LB_PresenciaLluvia.Text = "label6";
            // 
            // LB_TemperaturaAmbiente
            // 
            this.LB_TemperaturaAmbiente.AutoSize = true;
            this.LB_TemperaturaAmbiente.Location = new System.Drawing.Point(153, 136);
            this.LB_TemperaturaAmbiente.Name = "LB_TemperaturaAmbiente";
            this.LB_TemperaturaAmbiente.Size = new System.Drawing.Size(35, 13);
            this.LB_TemperaturaAmbiente.TabIndex = 6;
            this.LB_TemperaturaAmbiente.Text = "label7";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(27, 189);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(128, 13);
            this.label8.TabIndex = 6;
            this.label8.Text = "Estado del mar (Douglas):";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(49, 171);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(106, 13);
            this.label9.TabIndex = 6;
            this.label9.Text = "Velocidad del viento:";
            // 
            // LB_TemperaturaMar
            // 
            this.LB_TemperaturaMar.AutoSize = true;
            this.LB_TemperaturaMar.Location = new System.Drawing.Point(153, 154);
            this.LB_TemperaturaMar.Name = "LB_TemperaturaMar";
            this.LB_TemperaturaMar.Size = new System.Drawing.Size(41, 13);
            this.LB_TemperaturaMar.TabIndex = 6;
            this.LB_TemperaturaMar.Text = "label10";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(52, 241);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(104, 13);
            this.label11.TabIndex = 6;
            this.label11.Text = "Presencia de lluvias:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(50, 224);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(108, 13);
            this.label12.TabIndex = 6;
            this.label12.Text = "Especie de medusas:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(40, 207);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(117, 13);
            this.label13.TabIndex = 6;
            this.label13.Text = "Presencia de medusas:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(46, 154);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(108, 13);
            this.label14.TabIndex = 6;
            this.label14.Text = "Temepratura del Mar:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(38, 136);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(116, 13);
            this.label15.TabIndex = 6;
            this.label15.Text = "Temperatura ambiente:";
            // 
            // LB_UltimaActualizacion
            // 
            this.LB_UltimaActualizacion.AutoSize = true;
            this.LB_UltimaActualizacion.Location = new System.Drawing.Point(153, 75);
            this.LB_UltimaActualizacion.Name = "LB_UltimaActualizacion";
            this.LB_UltimaActualizacion.Size = new System.Drawing.Size(41, 13);
            this.LB_UltimaActualizacion.TabIndex = 6;
            this.LB_UltimaActualizacion.Text = "label16";
            // 
            // BT_ActualizarDatos
            // 
            this.BT_ActualizarDatos.Location = new System.Drawing.Point(90, 19);
            this.BT_ActualizarDatos.Name = "BT_ActualizarDatos";
            this.BT_ActualizarDatos.Size = new System.Drawing.Size(64, 41);
            this.BT_ActualizarDatos.TabIndex = 7;
            this.BT_ActualizarDatos.Text = "Actualizar Datos";
            this.BT_ActualizarDatos.UseVisualStyleBackColor = true;
            this.BT_ActualizarDatos.Click += new System.EventHandler(this.BT_ActualizarDatos_Click);
            // 
            // EstadoDelMar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(765, 484);
            this.Controls.Add(this.BT_ActualizarDatos);
            this.Controls.Add(this.LB_UltimaActualizacion);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.LB_TemperaturaMar);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.LB_TemperaturaAmbiente);
            this.Controls.Add(this.LB_PresenciaLluvia);
            this.Controls.Add(this.LB_EstadoMar);
            this.Controls.Add(this.LB_PresenciaMedusas);
            this.Controls.Add(this.LB_EspecieMedusas);
            this.Controls.Add(this.LB_VelocidadViento);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Historial);
            this.Name = "EstadoDelMar";
            this.Text = "Estado del Mar";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Historial.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BT_TemperaturaAmbiente;
        private System.Windows.Forms.Button BT_TemperaturaMar;
        private System.Windows.Forms.Button BT_EstadoMar;
        private System.Windows.Forms.Button BT_VelovidadViento;
        private System.Windows.Forms.Button BT_PresenciaMedusas;
        private System.Windows.Forms.Button BT_PresenciaLluvias;
        private System.Windows.Forms.GroupBox Historial;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label LB_VelocidadViento;
        private System.Windows.Forms.Label LB_EspecieMedusas;
        private System.Windows.Forms.Label LB_PresenciaMedusas;
        private System.Windows.Forms.Label LB_EstadoMar;
        private System.Windows.Forms.Label LB_PresenciaLluvia;
        private System.Windows.Forms.Label LB_TemperaturaAmbiente;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label LB_TemperaturaMar;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label LB_UltimaActualizacion;
        private System.Windows.Forms.Button BT_ActualizarDatos;
    }
}

