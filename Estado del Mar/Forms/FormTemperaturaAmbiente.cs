﻿using Estado_del_Mar.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Linq;
using System.Windows.Forms.DataVisualization.Charting;

namespace Estado_del_Mar
{

    public partial class FormTemperaturaAmbiente : Form
    {
        private List<Muestra> Muestras { get; set; }
        public FormTemperaturaAmbiente(List<Muestra> muestras)
        {
            InitializeComponent();
            this.Muestras = muestras;
        }

        private void FormTemperatura_Load(object sender, EventArgs e)
        {
            List<GridDto<int>> gridDto = (from muestra in Muestras
                                          select new GridDto<int>()
                                          {
                                              Fecha = muestra.Fecha,
                                              Value = muestra.TempAmbiente
                                          }).ToList();

            DGV_Temperatuda.DataSource = gridDto;
            DGV_Temperatuda.Columns[1].HeaderText = "Temperatura (ºC)";
            RellenarGrafica(Muestras[Muestras.Count() - 2].Fecha);
            DTP_TemperaturaAmbiente.Value = Muestras[Muestras.Count() - 2].Fecha;

        }

        private void RellenarGrafica(DateTime fecha)
        {
            List<Muestra> muestrasMes = Muestras.Where(w => w.Fecha.Month == fecha.Month && w.Fecha.Year == fecha.Year).ToList();

            C_TemperaturaAmbiente.Series.Clear();
            Series series = C_TemperaturaAmbiente.Series.Add("Temperatura");
            series.ChartType = SeriesChartType.Spline;
            for (int i = 0; i < muestrasMes.Count(); i++)
            {
                series.Points.AddXY(muestrasMes.ElementAt(i).Fecha, muestrasMes.ElementAt(i).TempAmbiente);
            }
            if (muestrasMes.Any())
            {
                C_TemperaturaAmbiente.ChartAreas[0].AxisY.Maximum = muestrasMes.Max(m => m.TempAmbiente + 5);
                C_TemperaturaAmbiente.ChartAreas[0].AxisY.Minimum = muestrasMes.Min(m => m.TempAmbiente - 5);
            }
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            RellenarGrafica(DTP_TemperaturaAmbiente.Value);
            CalcularDatosEstadisticos(DTP_TemperaturaAmbiente.Value);
        }

        private void CalcularDatosEstadisticos(DateTime fecha)
        {
            List<Muestra> muestrasMes = Muestras.Where(w => w.Fecha.Month == fecha.Month && w.Fecha.Year == fecha.Year).ToList();
            if (muestrasMes.Any())
            {
                LB_Maximo.Text = muestrasMes.Max(m => m.TempAmbiente).ToString();
                LB_Minimo.Text = muestrasMes.Min(m => m.TempAmbiente).ToString();
                LB_Media.Text = muestrasMes.Average(m => m.TempAmbiente).ToString("#.##");
                LB_Moda.Text = (muestrasMes.Select(s => s.TempAmbiente)
                  .GroupBy(item => item)
                  .OrderByDescending(item => item.Count())
                  .First().Key).ToString();
            }
            else
            {
                LB_Maximo.Text = "0";
                LB_Minimo.Text = "0";
                LB_Media.Text = "0";
                LB_Moda.Text = "0";
            }
        }
    }
}
