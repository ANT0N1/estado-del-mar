﻿using Estado_del_Mar.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Linq;
using System.Windows.Forms.DataVisualization.Charting;

namespace Estado_del_Mar
{

    public partial class FormPresenciaMedusas : Form
    {
        private List<Muestra> Muestras { get; set; }
        public FormPresenciaMedusas(List<Muestra> muestras)
        {
            InitializeComponent();
            this.Muestras = muestras;
        }

        private void FormTemperatura_Load(object sender, EventArgs e)
        {
            var gridDto = (from muestra in Muestras
                           select new
                           {
                               Fecha = muestra.Fecha,
                               Value = muestra.PresenciaMedusas,
                               Especie = muestra.EspeciesMedusas
                           }).ToList();

            DGV_PresenciaMedusas.DataSource = gridDto;
            DGV_PresenciaMedusas.Columns[1].HeaderText = "Presencia";

            RellenarGrafica(Muestras[Muestras.Count() - 2].Fecha);
            DTP_PresenciaMedusas.Value = Muestras[Muestras.Count() - 2].Fecha;

        }

        private void RellenarGrafica(DateTime fecha)
        {
            List<Muestra> muestrasMes = Muestras.Where(w => w.Fecha.Month == fecha.Month && w.Fecha.Year == fecha.Year).ToList();

            C_PresenciaMedusas.Series.Clear();
            Series series = C_PresenciaMedusas.Series.Add("Temperatura");
            series.ChartType = SeriesChartType.Spline;
            for (int i = 0; i < muestrasMes.Count(); i++)
            {
                series.Points.AddXY(muestrasMes.ElementAt(i).Fecha, muestrasMes.ElementAt(i).PresenciaMedusas);
            }
            if (muestrasMes.Any())
            {
                C_PresenciaMedusas.ChartAreas[0].AxisY.Maximum = 5;
                C_PresenciaMedusas.ChartAreas[0].AxisY.Minimum = 0;
            }
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            RellenarGrafica(DTP_PresenciaMedusas.Value);
            CalcularDatosEstadisticos(DTP_PresenciaMedusas.Value);
        }
        private void CalcularDatosEstadisticos(DateTime fecha)
        {
            List<Muestra> muestrasMes = Muestras.Where(w => w.Fecha.Month == fecha.Month && w.Fecha.Year == fecha.Year).ToList();

            if (muestrasMes.Any())
            {
                LB_Maximo.Text = muestrasMes.Max(m => m.PresenciaMedusas).ToString();
                LB_Minimo.Text = muestrasMes.Min(m => m.PresenciaMedusas).ToString();
                LB_Media.Text = muestrasMes.Average(m => m.PresenciaMedusas).ToString("#.##");
                LB_Moda.Text = (muestrasMes.Select(s => s.PresenciaMedusas)
                  .GroupBy(item => item)
                  .OrderByDescending(item => item.Count())
                  .First().Key).ToString();
                LB_ModaEspecie.Text = (muestrasMes.Select(s => s.EspeciesMedusas)
                  .GroupBy(item => item)
                  .OrderByDescending(item => item.Count())
                  .First().Key).ToString();
            }
            else
            {
                LB_Maximo.Text = "0";
                LB_Minimo.Text = "0";
                LB_Media.Text = "0";
                LB_Moda.Text = "0";
                LB_ModaEspecie.Text = string.Empty;
            }
        }
    }
}
