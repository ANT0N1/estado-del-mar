﻿using Estado_del_Mar.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Linq;
using System.Windows.Forms.DataVisualization.Charting;

namespace Estado_del_Mar
{

    public partial class FormPresenciaLluvias : Form
    {
        private List<Muestra> Muestras { get; set; }
        public FormPresenciaLluvias(List<Muestra> muestras)
        {
            InitializeComponent();
            this.Muestras = muestras;
        }

        private void FormTemperatura_Load(object sender, EventArgs e)
        {
            var gridDto = (from muestra in Muestras
                           select new
                           {
                               Fecha = muestra.Fecha,
                               Value = muestra.Precipitaciones
                           }).ToList();

            DGV_PresenciaLluvias.DataSource = gridDto;
            DGV_PresenciaLluvias.Columns[1].HeaderText = "Precipitaciones (L/m2)";

            RellenarGrafica(Muestras[Muestras.Count() - 2].Fecha);
            DTP_PresenciaLluvias.Value = Muestras[Muestras.Count() - 2].Fecha;

        }

        private void RellenarGrafica(DateTime fecha)
        {
            List<Muestra> muestrasMes = Muestras.Where(w => w.Fecha.Month == fecha.Month && w.Fecha.Year == fecha.Year).ToList();

            C_PresenciaLluvias.Series.Clear();
            Series series = C_PresenciaLluvias.Series.Add("Temperatura");
            series.ChartType = SeriesChartType.Spline;
            for (int i = 0; i < muestrasMes.Count(); i++)
            {
                series.Points.AddXY(muestrasMes.ElementAt(i).Fecha, muestrasMes.ElementAt(i).Precipitaciones);
            }
            if (muestrasMes.Any())
            {
                C_PresenciaLluvias.ChartAreas[0].AxisY.Maximum = muestrasMes.Max(m => m.Precipitaciones + 5);
                C_PresenciaLluvias.ChartAreas[0].AxisY.Minimum = 0;
            }
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            RellenarGrafica(DTP_PresenciaLluvias.Value);
            CalcularDatosEstadisticos(DTP_PresenciaLluvias.Value);

        }

        private void CalcularDatosEstadisticos(DateTime fecha)
        {
            List<Muestra> muestrasMes = Muestras.Where(w => w.Fecha.Month == fecha.Month && w.Fecha.Year == fecha.Year).ToList();

            if (muestrasMes.Any())
            {
                LB_Maximo.Text = muestrasMes.Max(m => m.Precipitaciones).ToString();
                LB_Minimo.Text = muestrasMes.Min(m => m.Precipitaciones).ToString();
                LB_Media.Text = muestrasMes.Average(m => m.Precipitaciones).ToString("#.##");
                LB_Moda.Text = (muestrasMes.Select(s => s.Precipitaciones)
                  .GroupBy(item => item)
                  .OrderByDescending(item => item.Count())
                  .First().Key).ToString();
            }
            else
            {
                LB_Maximo.Text = "0";
                LB_Minimo.Text = "0";
                LB_Media.Text = "0";
                LB_Moda.Text = "0";
            }
        }
    }
}
