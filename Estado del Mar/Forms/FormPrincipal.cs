﻿using Estado_del_Mar.Dto;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Estado_del_Mar
{
    public partial class EstadoDelMar : Form
    {
        public List<Muestra> Muestras { get; set; }
        public Muestra UltimaMuestra { get; set; }
        public EstadoDelMar()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            ActualizarListaMuestras();
            ActualizarUltimaMuesta();
            ActualizarLabelsUltimaMuestra();
        }

        private void ActualizarListaMuestras()
        {
            string path = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), @"Data\Datos.json");
            using (StreamReader jsonStream = File.OpenText(path))
            {
                var json = jsonStream.ReadToEnd();
                Muestras = JsonConvert.DeserializeObject<List<Muestra>>(json).OrderBy(o => o.Fecha).ToList();
            }
        }

        private void ActualizarUltimaMuesta()
        {
            UltimaMuestra = Muestras.ElementAt(Muestras.Count() - 1);
        }

        private void ActualizarLabelsUltimaMuestra()
        {
            LB_EspecieMedusas.Text = UltimaMuestra.EspeciesMedusas;
            LB_EstadoMar.Text = UltimaMuestra.EstadoMarDouglas.ToString();
            LB_PresenciaLluvia.Text = UltimaMuestra.Precipitaciones.ToString() + " L/m2";
            switch (UltimaMuestra.PresenciaMedusas)
            {
                case 1:
                    LB_PresenciaMedusas.Text = "1: Baja presencia";
                    break;
                case 2:
                    LB_PresenciaMedusas.Text = "2: Frecuentes en algunos momentos";
                    break;
                case 3:
                    LB_PresenciaMedusas.Text = "3: Aparición de enjambres";
                    break;
                default:
                    LB_PresenciaMedusas.Text = "No data";
                    break;

            }
            LB_TemperaturaAmbiente.Text = UltimaMuestra.TempAmbiente.ToString() + " ºC";
            LB_TemperaturaMar.Text = UltimaMuestra.TempMar.ToString() + " ºC";
            LB_UltimaActualizacion.Text = UltimaMuestra.Fecha.ToString("dd'/'MM'/'yyyy HH:mm");
            LB_VelocidadViento.Text = UltimaMuestra.VelocidadViento.ToString() + " km/hora";
        }

        private void BT_ActualizarDatos_Click(object sender, EventArgs e)
        {
            ActualizarListaMuestras();
            ActualizarUltimaMuesta();
            ActualizarLabelsUltimaMuestra();
        }

        private void BT_TemperaturaAmbiente_Click(object sender, EventArgs e)
        {
            Form formTemperatura = new FormTemperaturaAmbiente(Muestras);

            formTemperatura.Show();
        }

        private void BT_TemperaturaMar_Click(object sender, EventArgs e)
        {
            Form formTemperaturaMar = new FormTemperaturaMar(Muestras);

            formTemperaturaMar.Show();
        }

        private void BT_VelovidadViento_Click(object sender, EventArgs e)
        {
            Form formVelocidadViento = new FormVelocidadViento(Muestras);

            formVelocidadViento.Show();
        }

        private void BT_EstadoMar_Click(object sender, EventArgs e)
        {
            Form formEstadoMar = new FormEstadoMar(Muestras);
            formEstadoMar.Show();
        }

        private void BT_PresenciaMedusas_Click(object sender, EventArgs e)
        {
            Form formPresenciaMedusas = new FormPresenciaMedusas(Muestras);
            formPresenciaMedusas.Show();
        }

        private void BT_PresenciaLluvias_Click(object sender, EventArgs e)
        {
            Form formPresenciaLluvias = new FormPresenciaLluvias(Muestras);
            formPresenciaLluvias.Show();
        }
    }
}
