﻿using Estado_del_Mar.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Linq;
using System.Windows.Forms.DataVisualization.Charting;

namespace Estado_del_Mar
{

    public partial class FormEstadoMar : Form
    {
        private List<Muestra> Muestras { get; set; }
        public FormEstadoMar(List<Muestra> muestras)
        {
            InitializeComponent();
            this.Muestras = muestras;
        }

        private void FormTemperatura_Load(object sender, EventArgs e)
        {
            var gridDto = (from muestra in Muestras
                           select new
                           {
                               Fecha = muestra.Fecha,
                               Value = muestra.EstadoMarDouglas
                           }).ToList();

            DGV_EstadoMar.DataSource = gridDto;
            DGV_EstadoMar.Columns[1].HeaderText = "Estado del mar (Douglas)";

            RellenarGrafica(Muestras[Muestras.Count() - 2].Fecha);
            DTP_EstadoMar.Value = Muestras[Muestras.Count() - 2].Fecha;

        }

        private void RellenarGrafica(DateTime fecha)
        {
            List<Muestra> muestrasMes = Muestras.Where(w => w.Fecha.Month == fecha.Month && w.Fecha.Year == fecha.Year).ToList();

            C_EstadoMar.Series.Clear();
            Series series = C_EstadoMar.Series.Add("Temperatura");
            series.ChartType = SeriesChartType.Line;
            for (int i = 0; i < muestrasMes.Count(); i++)
            {
                series.Points.AddXY(muestrasMes.ElementAt(i).Fecha, muestrasMes.ElementAt(i).EstadoMarDouglas);
            }
            if (muestrasMes.Any())
            {
                C_EstadoMar.ChartAreas[0].AxisY.Maximum = 10;
                C_EstadoMar.ChartAreas[0].AxisY.Minimum = 0;
            }
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            RellenarGrafica(DTP_EstadoMar.Value);
            CalcularDatosEstadisticos(DTP_EstadoMar.Value);
        }

        private void CalcularDatosEstadisticos(DateTime fecha)
        {
            List<Muestra> muestrasMes = Muestras.Where(w => w.Fecha.Month == fecha.Month && w.Fecha.Year == fecha.Year).ToList();

            if (muestrasMes.Any())
            {
                LB_Maximo.Text = muestrasMes.Max(m => m.EstadoMarDouglas).ToString();
                LB_Minimo.Text = muestrasMes.Min(m => m.EstadoMarDouglas).ToString();
                LB_Media.Text = muestrasMes.Average(m => m.EstadoMarDouglas).ToString("#.##");
                LB_Moda.Text = (muestrasMes.Select(s => s.EstadoMarDouglas)
                  .GroupBy(item => item)
                  .OrderByDescending(item => item.Count())
                  .First().Key).ToString();
            }
            else
            {
                LB_Maximo.Text = "0";
                LB_Minimo.Text = "0";
                LB_Media.Text = "0";
                LB_Moda.Text = "0";
            }
        }
    }
}
