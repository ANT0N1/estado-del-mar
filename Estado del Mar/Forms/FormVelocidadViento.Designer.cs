﻿namespace Estado_del_Mar
{
    partial class FormVelocidadViento
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.DGV_VelocidadViento = new System.Windows.Forms.DataGridView();
            this.C_VelocidadViento = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.DTP_VelocidadViento = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.LB_Moda = new System.Windows.Forms.Label();
            this.LB_Media = new System.Windows.Forms.Label();
            this.LB_Minimo = new System.Windows.Forms.Label();
            this.LB_Maximo = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.DGV_VelocidadViento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.C_VelocidadViento)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // DGV_VelocidadViento
            // 
            this.DGV_VelocidadViento.AllowUserToAddRows = false;
            this.DGV_VelocidadViento.AllowUserToDeleteRows = false;
            this.DGV_VelocidadViento.AllowUserToOrderColumns = true;
            this.DGV_VelocidadViento.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.DGV_VelocidadViento.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGV_VelocidadViento.Location = new System.Drawing.Point(10, 151);
            this.DGV_VelocidadViento.MultiSelect = false;
            this.DGV_VelocidadViento.Name = "DGV_VelocidadViento";
            this.DGV_VelocidadViento.ReadOnly = true;
            this.DGV_VelocidadViento.Size = new System.Drawing.Size(261, 367);
            this.DGV_VelocidadViento.TabIndex = 0;
            this.DGV_VelocidadViento.Text = "dataGridView1";
            // 
            // C_VelocidadViento
            // 
            this.C_VelocidadViento.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            chartArea1.Name = "ChartArea1";
            this.C_VelocidadViento.ChartAreas.Add(chartArea1);
            this.C_VelocidadViento.Location = new System.Drawing.Point(277, 151);
            this.C_VelocidadViento.Name = "C_VelocidadViento";
            series1.ChartArea = "ChartArea1";
            series1.Name = "Series1";
            this.C_VelocidadViento.Series.Add(series1);
            this.C_VelocidadViento.Size = new System.Drawing.Size(516, 366);
            this.C_VelocidadViento.TabIndex = 1;
            this.C_VelocidadViento.Text = "chart1";
            // 
            // DTP_VelocidadViento
            // 
            this.DTP_VelocidadViento.Location = new System.Drawing.Point(377, 20);
            this.DTP_VelocidadViento.Name = "DTP_VelocidadViento";
            this.DTP_VelocidadViento.Size = new System.Drawing.Size(200, 20);
            this.DTP_VelocidadViento.TabIndex = 2;
            this.DTP_VelocidadViento.Tag = "";
            this.DTP_VelocidadViento.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(79, 132);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Histórico completo";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(286, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Filtro gráfica";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(608, 26);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(185, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "*Solo se tiene en cuenta el mes y año";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.LB_Moda);
            this.groupBox1.Controls.Add(this.LB_Media);
            this.groupBox1.Controls.Add(this.LB_Minimo);
            this.groupBox1.Controls.Add(this.LB_Maximo);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Location = new System.Drawing.Point(277, 58);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(516, 87);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Estadísticas mes seleccionado";
            // 
            // LB_Moda
            // 
            this.LB_Moda.AutoSize = true;
            this.LB_Moda.Location = new System.Drawing.Point(271, 46);
            this.LB_Moda.Name = "LB_Moda";
            this.LB_Moda.Size = new System.Drawing.Size(34, 13);
            this.LB_Moda.TabIndex = 7;
            this.LB_Moda.Text = "Moda";
            // 
            // LB_Media
            // 
            this.LB_Media.AutoSize = true;
            this.LB_Media.Location = new System.Drawing.Point(271, 30);
            this.LB_Media.Name = "LB_Media";
            this.LB_Media.Size = new System.Drawing.Size(36, 13);
            this.LB_Media.TabIndex = 6;
            this.LB_Media.Text = "Media";
            // 
            // LB_Minimo
            // 
            this.LB_Minimo.AutoSize = true;
            this.LB_Minimo.Location = new System.Drawing.Point(76, 44);
            this.LB_Minimo.Name = "LB_Minimo";
            this.LB_Minimo.Size = new System.Drawing.Size(42, 13);
            this.LB_Minimo.TabIndex = 5;
            this.LB_Minimo.Text = "Mínimo";
            // 
            // LB_Maximo
            // 
            this.LB_Maximo.AutoSize = true;
            this.LB_Maximo.Location = new System.Drawing.Point(75, 27);
            this.LB_Maximo.Name = "LB_Maximo";
            this.LB_Maximo.Size = new System.Drawing.Size(43, 13);
            this.LB_Maximo.TabIndex = 4;
            this.LB_Maximo.Text = "Máximo";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(231, 46);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(34, 13);
            this.label7.TabIndex = 3;
            this.label7.Text = "Moda";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(231, 30);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(36, 13);
            this.label6.TabIndex = 2;
            this.label6.Text = "Media";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(26, 44);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(42, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "Mínimo";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(26, 27);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(43, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Máximo";
            // 
            // FormVelocidadViento
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(805, 529);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.DTP_VelocidadViento);
            this.Controls.Add(this.C_VelocidadViento);
            this.Controls.Add(this.DGV_VelocidadViento);
            this.Name = "FormVelocidadViento";
            this.Text = "Velocidad del viento";
            this.Load += new System.EventHandler(this.FormTemperatura_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DGV_VelocidadViento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.C_VelocidadViento)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.DataGridView DGV_VelocidadViento;
        private System.Windows.Forms.DataVisualization.Charting.Chart C_VelocidadViento;
        private System.Windows.Forms.DateTimePicker DTP_VelocidadViento;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label LB_Moda;
        private System.Windows.Forms.Label LB_Media;
        private System.Windows.Forms.Label LB_Minimo;
        private System.Windows.Forms.Label LB_Maximo;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
    }
}