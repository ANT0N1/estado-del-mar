﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Estado_del_Mar.Dto
{
    public class GridDto<T>
    {
        public DateTime Fecha { get; set; }
        public T Value { get; set; }
    }
}
