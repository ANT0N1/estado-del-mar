﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Estado_del_Mar.Dto
{
    public class Muestra
    {
        public DateTime Fecha { get; set; }
        public int TempAmbiente { get; set; }
        public int TempMar { get; set; }
        public int VelocidadViento { get; set; }
        public int EstadoMarDouglas { get; set; }
        public int PresenciaMedusas { get; set; }
        public string EspeciesMedusas { get; set; }
        public int Precipitaciones { get; set; }
    }
}
